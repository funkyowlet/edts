import time

"""
This module provides a simple metrics implementation for runtime information gathering.
"""


class BaseMetric(object):
  """
  Simple base metric
  """

  def __init__(self, name):
    self.name = name

  def update(self, value):
    pass

  def reset(self):
    pass


class ObjectMetric(BaseMetric):
  """
  Simple base metric with object retention
  """

  def __init__(self, name, max_objects=10000000):
    super(ObjectMetric, self).__init__(name)

    assert max_objects > 0

    self.objects = []
    self.max_objects = max_objects

  def update(self, value):
    self.objects.append(value)

    while len(self.objects > self.max_objects):
      self.objects.pop(0)

  def reset(self):
    self.objects = []


class Counter(BaseMetric):
  """
  Simple counter that supports updates with arbitrary values.
  """

  def __init__(self, name, value=0):
    super(Counter, self).__init__(name)
    self.__value = value
    self.name = name

  def update(self, values=1):
    self.__value += values

  def inc(self, values=1):
    self.update(values)

  def reset(self):
    val = self.__value
    self.__value = 0
    return val

  def value(self):
    return self.__value

  def __repr__(self):
    return "Counter(" + str(self.name) + ")[" + str(self.__value) + "]"


class ObjectCollector(Counter):
  """
  This metric collects object up to a certain limit.
  """

  def __init__(self, name, objects=[], max_objects=10000000):
    super(ObjectCollector, self).__init__(name, 0)
    self.objects = objects
    self.max_objects = max_objects

  def update(self, object):
    self.objects.append(object)
    super(ObjectCollector, self).update()

    while len(self.objects) > self.max_objects:
      self.objects.pop(0)


class Timer(BaseMetric):
  def __init__(self, name):
    super(Timer, self).__init__(name)
    self.start_time = time.time()

  def start(self):
    self.start = time.time()

  def time(self):
    return time.time() - self.start_time

  def reset(self):
    self.start()


class AverageTimer(Timer):
  def __init__(self, name):
    super(AverageTimer, self).__init__(name)
    self.cumulative = 0
    self.counted = 0

  def average(self):
    if self.counted == 0:
      return 0
    else:
      return self.cumulative / self.counted

  def time(self):
    time = super(AverageTimer, self).time()
    super(AverageTimer, self).reset()
    self.counted += 1
    self.cumulative += time
    return time


class AverageIntervalTimer(BaseMetric):
  def __init__(self, name, intervals=[1, 10, 30, 60], evictcycle=10000, hardevictlimit=100000):
    super(AverageIntervalTimer, self).__init__(name)
    self.tickbuckets = []
    self.intervals = sorted(intervals)
    self.evictcycle = evictcycle
    self.hardevictlimit = hardevictlimit
    self.polls = 0

  def update(self, value=1):
    self.tickbuckets.append(time.time())

  def time(self):
    self.polls += 1
    timestamp = time.time()
    buckets = dict()

    for interval in self.intervals:
      buckets[interval] = 0

    for buckettime in self.tickbuckets:
      for interval in self.intervals:
        delta = timestamp - buckettime
        if delta <= interval:
          buckets[interval] += 1

    for interval in self.intervals:
      buckets[interval] /= interval

    if self.polls >= self.evictcycle:
      self.polls = 0
      self.cleanBuckets()

    if len(self.tickbuckets) > self.hardevictlimit:
      smallest_bucket = min(self.intervals)
      self.tickbuckets = list(filter(lambda buckettime: timestamp - buckettime <= smallest_bucket, self.tickbuckets))

    return buckets

  def cleanBuckets(self):
    timestamp = time.time()
    largest_bucket = max(self.intervals)

    self.tickbuckets = list(filter(lambda buckettime: timestamp - buckettime <= largest_bucket, self.tickbuckets))


class EventLogger(ObjectCollector):
  def __init__(self, name, handler=None):
    super(EventLogger, self).__init__(name)
    self.handler = handler

  def update(self, value):
    super(EventLogger, self).update(value)
    if self.handler is not None:
      self.handler(value)
    else:
      pass


class Metrics:
  def __init__(self, defaultType=None):
    self.__metrics = dict()
    self.__defaultType = defaultType

  def new(self, name, type):
    return self[name, type]

  def discard(self, name):
    del self.__metrics[name]

  def __getitem__(self, arg):
    name = None
    metricType = None
    if isinstance(arg, tuple):
      if len(arg) > 1:
        metricType = arg[1]
      name = arg[0]
    else:
      name = arg
    if not name in self.__metrics:
      if metricType is None:
        raise ValueError("Name " + str(name) + " not declared.")
      else:
        self.__metrics[name] = metricType(name)

    return self.__metrics[name]

  def __repr__(self):
    return str(self.__metrics)


if __name__ == '__main__':
  ctr = Counter("test_ctr")
  print ctr
  ctr.update(12)
  print ctr
  ctr.reset()
  print ctr
  ctr.update()
  print ctr.value()

  ticker = AverageIntervalTimer("test", [1, 5, 10], hardevictlimit=1000000)

  start = time.time()

  cycle = 0
  while True:  # time.time() - start < 20:
    ticker.update()
    cycle += 1
    if cycle > 10000:
      print ticker.time()
      cycle = 0
