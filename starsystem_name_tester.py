import argparse
import multiprocessing
import traceback
import urllib2

import ijson.backends.yajl2_cffi as ijson

from metrics import *
from pgnames_test_cleaned import test_system

"""
A small test rig to leverage multiple cores and asynchronous calls.
"""


class Position:
  """
  A simple class to handle positions in 3D euclidian space.
  """

  def __init__(self, x=None, y=None, z=None):
    """
    The position can be constructed without arguments and later be filled up with valid positions.
    :param x: X position (must be float)
    :param y: Y Position (must be float)
    :param z: Z Position (must be float)
    """
    assert x is None or isinstance(x, float), "X Coordinate for Position object must be float or None."
    assert y is None or isinstance(y, float), "Y Coordinate for Position object must be float or None."
    assert z is None or isinstance(z, float), "Z Coordinate for Position object must be float or None."

    self.x = x
    self.y = y
    self.z = z

  @property
  def x(self):
    return self.x

  @x.setter
  def x(self, value):
    assert value is not None and isinstance(value, float), "X Coordinate for Position object must be float."
    self.x = value

  @property
  def y(self):
    return self.y

  @y.setter
  def y(self, value):
    assert value is not None and isinstance(value, float), "Y Coordinate for Position object must be float."
    self.y = value

  @property
  def z(self):
    return self.z

  @z.setter
  def z(self, value):
    assert value is not None and isinstance(value, float), "X Coordinate for Position object must be float."
    self.z = value

  def is_valid(self):
    return self.x is not None and self.y is not None and self.z is not None

  def __iter__(self):
    return [self.x, self.y, self.z].__iter__()


class Starsystem:
  def __init__(self, name=None, position=None, id=None, date=None):
    self.name = name
    if position is None:
      self.position = Position()
    else:
      self.position = position
    self.id = id
    self.date = date

  def __str__(self):
    return "S[" + str(self.name) + ", x: " + str(self.x) + ", y: " + str(self.y) + ", z: " + str(
      self.z) + ", id: " + str(self.id) + " / " + str(self.date)

  def is_valid(self):
    return self.name is not None and self.id is not None and self.date is not None and self.position.is_valid()


def main(filename):
  metrics = Metrics(ObjectCollector)
  start = time.time()
  fullstart = time.time()
  pool = multiprocessing.Pool(processes=multiprocessing.cpu_count() - 1, maxtasksperchild=1000)

  print "Declaring multiprocessing pool. (", (multiprocessing.cpu_count() - 1), " Workers)"

  START_SIG = ('item', 'start_map', None)
  END_SIG = ('item', 'end_map', None)
  limit = 1000000

  def error_log(e):
    pass

  # metrics.newCounter("systems_parsed")
  # metrics.newCountingTimer("systems_per_second")
  # metrics.newLogger("exception", error_log)

  # This is the logging callback.
  def check_callback(result):
    if isinstance(result, list):
      for value in result:
        metrics[value[0], value[1]].update(value[2:])
    elif isinstance(result, tuple):
      metrics[result[0], result[1]].update(result[2:])
    elif isinstance(result, Exception):
      traceback.print_exc(result)

  with open(filename, 'r') as jsonfile:
    for value in ijson.parse(jsonfile):
      if time.time() - start >= 10:
        print "Metrics: " + str(metrics)

        if metrics["systems_parsed"].value() >= limit:
          break

        start = time.time()

      if value == START_SIG:
        starsystem = Starsystem()

      elif value == END_SIG:
        metrics["systems_parsed", Counter].inc()
        metrics["systems_per_second", Counter].inc()

        if not starsystem.is_valid():
          print "Invalid starsystem parsed.", str(starsystem)
        else:
          # pool.apply(check,(starsystem, ))
          pool.apply_async(check, args=(starsystem,), callback=check_callback)
          # check(starsystem, flightrecorder)

        starsystem = None
      elif value[1] == 'number' or value[1] == 'string':
        if value[0] == 'item.coords.x':
          starsystem.position.x = float(value[2])
        elif value[0] == 'item.coords.y':
          starsystem.position.y = float(value[2])
        elif value[0] == 'item.coords.z':
          starsystem.position.z = float(value[2])

        elif value[0] == 'item.name':
          starsystem.name = value[2]
        elif value[0] == 'item.id':
          starsystem.id = value[2]
        elif value[0] == 'item.date':
          starsystem.date = value[2]

          # print "Parsed ", flightrecorder.items, " objects in ", (time.time() - fullstart), "s"


def check(system):
  output = list()
  try:
    test_system(system, output)
  except Exception as e:
    output.append(("exception", EventLogger, e))

  return output


class Args(object):
  def __init__(self):
    self.download = None
    self.file = None
    self.cached = None

  def __repr__(self):
    return str([self.download, self.file, self.cached])


def download(url):
  file_name = url.split('/')[-1]
  u = urllib2.urlopen(url)
  f = open(file_name, 'wb')
  meta = u.info()
  file_size = 1
  try:
    file_size = int(meta.getheaders("Content-Length")[0])
    print "Downloading: %s Bytes: %s" % (file_name, file_size)
  except:
    pass

  file_size_dl = 0
  block_sz = 8192
  while True:
    buffer = u.read(block_sz)
    if not buffer:
      break

    file_size_dl += len(buffer)
    f.write(buffer)
    status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
    status = status + chr(8) * (len(status) + 1)
    print status,

  f.close()

  return file_name


if __name__ == '__main__':
  argparser = argparse.ArgumentParser(description="Parallelized pgnames test rig. (with metrics)")
  group = argparser.add_mutually_exclusive_group()
  group.required = True
  group.add_argument('--download', '-d', metavar='d', nargs='?', type=str, action='store',
                     help='Fetch a fresh copy of the starsystems json.',
                     const='https://www.edsm.net/dump/systemsWithCoordinates.json',
                     dest='download'
                     )
  group.add_argument('--file', '-f', metavar='f', help='Use the provided starsystems.json.', nargs='?',
                     const='systemsWithCoordinates.json',
                     action='store',
                     type=str,
                     dest='file'
                     )

  args = Args()

  argparser.parse_args(namespace=args)

  if args.download:
    print "Downloading new copy."
    args.file = download(args.download)


  elif args.file:
    print "Using file: " + args.file

  main(args.file)
  print args
